#ifndef EXPRESSION_H
#define EXPRESSION_H
#include "list.h"
#include "postfix_expression.h"
#include "operator.h"
#include <iostream>

namespace Infix {
    typedef int Variable;
    typedef const Function* FunctionId;

    class Object {
        union {
            Variable v;
            Operator o;
            FunctionId f;
        } data;
        public:
        enum class Type {
            Variable,
            Operator,
            OpenBracket,
            CloseBracket,
            Function,
        } type;
        static Object new_function(FunctionId);
        static Object new_variable(Variable);
        static Object new_operator(Operator);
        static Object new_open_bracket();
        static Object new_close_bracket();
        bool is_function() const;
        bool is_variable() const;
        bool is_operator() const;
        bool is_open_bracket() const;
        bool is_close_bracket() const;
        bool is_plus() const;
        bool is_minus() const;
        const FunctionId&  get_function() const;
        Postfix::Object to_posfix_object() const;
        int precedence() const;
        friend std::ostream& operator << (std::ostream &, const Object &);
    };

    class InfixExpression {
        List<Object> list;
        public:
        InfixExpression(const List<Object> &&l);
        bool is_valid() const;
        Postfix::PostfixExpression to_posfix() const;
    };
};

#endif
