#include "src/calculator.h"
#include <iostream>
#include <fstream>
#include <time.h>
using std::cout;
using std::endl;

int main(int argc, char **argv) {
    if (argc >= 3) {
        std::ifstream is(argv[1]);
        std::ofstream os(argv[2]);
        if (!is || !os) {
            cout << "Cannot open files." << endl;
        } else {
            srand(time(NULL));
            Calculator calc;
            while (!is.eof()) {
                std::string input;
                getline(is, input);
                if (input.empty()) continue;
                try {
                    os << calc.evaluate(input.data()) << endl;
                } catch (Exception e) {
                    os << e << endl;
                }
            }
        }
    } else {
        cout << "No files name given." << endl;
    }
    return 0;
}
