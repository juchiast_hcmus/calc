#include "calculator.h"
#include "infix_expression.h"
#include "string.h"
#include <tuple>

using Infix::InfixExpression;
using Infix::Object;
using Postfix::PostfixExpression;

namespace calculator {
    bool is_variable(char c) {
        return 'A' <= c && c <= 'Z';
    }

    bool is_function(char c) {
        return 'a' <= c && c <= 'z';
    }

    bool is_number(char c) {
        return '0' <= c && c <= '9';
    }

    bool is_operator(char c) {
        return c=='+' || c=='/' || c=='-' || c=='*' || c=='^';
    }

    bool is_valid(char c) {
        return is_number(c) || is_function(c) || is_operator(c) || is_variable(c)
            || c == '(' || c == ')' || c == '.'
            || c == '=';
    }

    bool is_whitespace(char c) {
        return c == ' ' || c == '\n' || c == '\t';
    }

    char* clean(const char *s) {
        for (const char *c=s; *c != '\0'; c++) {
            if (!is_valid(*c) && !is_whitespace(*c)) return nullptr;
        }
        int len = strlen(s);
        char *n = new char[len+1];
        int i = 0;
        for (const char *c=s; *c != '\0'; c++) {
            if (is_valid(*c)) {
                if (*c == '=') {
                    if (i != 1 || !is_variable(s[0])) {
                        delete[] n;
                        return nullptr;
                    }
                }
                n[i] = *c;
                i++;
            }
        }
        n[i] = '\0';
        return n;
    }
};

using namespace calculator;

const int NF = 15;

Calculator::Calculator() {
    for (int i=0; i<26; i++) var[i] = nullptr;
    functions = new Function[NF];
    functions[0] = Function::sqrt;
    functions[1] = Function::log;
    functions[2] = Function::fraction;
    functions[3] = Function::floor;
    functions[4] = Function::abs;
    functions[5] = Function::exp;
    functions[6] = Function::sin;
    functions[7] = Function::cos;
    functions[8] = Function::tan;
    functions[8] = Function::atan;
    functions[10] = Function::asin;
    functions[11] = Function::acos;
    functions[12] = Function::inv;
    functions[13] = Function::pi;
    functions[14] = Function::e;
}

Calculator::~Calculator() {
    for (int i=0; i<26; i++) delete var[i];
    delete[] functions;
}

int Calculator::find_function(int id) const {
    for (int i=0; i<NF; i++) if (functions[i].get_id() == id) return i;
    return -1;
}

List<Object> Calculator::fix(List<Object> &&list) const {
    if (list.empty()) return list;

    auto it = list.begin();
    while (it.next() != list.end()) {
        if ((*it).is_variable() && (*it.next()).is_variable()) {
            it.insert_after(Object::new_operator(Operator::Multiply));
        } else if ((*it).is_close_bracket() &&
            ((*it.next()).is_open_bracket() || (*it.next()).is_function())) {
            it.insert_after(Object::new_operator(Operator::Multiply));
        }
        it++;
    }

    if (list.empty()) return list;

    it = list.begin();
    while (it.next() != list.end() && it.next().next() != list.end()) {
        if ((*it).is_plus() && (*it.next()).is_plus()) {
            *it = Object::new_operator(Operator::Plus);
            it.remove_after();
        } else if ((*it).is_plus() && (*it.next()).is_minus()) {
            *it = Object::new_operator(Operator::Minus);
            it.remove_after();
        } else if ((*it).is_minus() && (*it.next()).is_minus()) {
            *it = Object::new_operator(Operator::Plus);
            it.remove_after();
        } else if ((*it).is_minus() && (*it.next()).is_plus()) {
            *it = Object::new_operator(Operator::Minus);
            it.remove_after();
        } else it++;
    }

    if (list.empty()) return list;

    if (list.front().is_plus()) list.pop_front();

    if (list.empty()) return list;

    it = list.begin();
    
    while (it.next() != list.end() && it.next().next() != list.end()) {
        if (((*it).is_operator() || (*it).is_open_bracket())
            && (*it.next()).is_plus()) {
            assert(!(*it).is_plus() && !(*it).is_minus());
            it.remove_after();
        } else it++;
    }

    if (list.empty()) return list;

    Object inv = Object::new_function(functions + find_function(Function::hash("inv")));
    if (list.front().is_minus()) list.front() = inv;

    if (list.empty()) return list;
    it = list.begin();

    while (it.next() != list.end() && it.next().next() != list.end()) {
        if (((*it).is_operator() || (*it).is_open_bracket())
            && (*it.next()).is_minus()) {
            assert(!(*it).is_plus() && !(*it).is_minus());
            *it.next() = inv;
        }
        it++;
    }

    return list;
}

std::tuple<InfixExpression, Map<Number>, char> Calculator::parse(const char *s) const {
    List<Object> list;
    Map<Number> map;
    char *buffer = new char[strlen(s)+1];
    int len = strlen(s);
    char c_var = '\0';
    int start = 0;
    if (s[1] == '=') {
        c_var = s[0];
        start = 2;
    }
    for (int i=start; i<len; i++) {
        char c = s[i];
        if (calculator::is_function(c)) {
            buffer[0] = c;
            int j = 1;
            while (i+1<len && calculator::is_function(s[i+1])) {
                buffer[j] = s[i+1];
                j++;
                i++;
            }
            buffer[j] = '\0';
            int id = find_function(Function::hash(buffer));
            if (id<0) throw Exception("Function not found");
            list.push_back(Object::new_function(functions + id));
            if (functions[id].get_count() == 0) {
                list.push_back(Object::new_open_bracket());
                list.push_back(Object::new_close_bracket());
            }
        } else if (is_variable(c)) {
            if (var[c-'A'] == nullptr) throw Exception("Variable is not initialized");
            list.push_back(Object::new_variable(map.insert(*(var[c-'A']))));
        } else if (c == '(') {
            list.push_back(Object::new_open_bracket());
        } else if (c == ')') {
            list.push_back(Object::new_close_bracket());
        } else if (c == '+') {
            list.push_back(Object::new_operator(Operator::Plus));
        } else if (c == '*') {
            list.push_back(Object::new_operator(Operator::Multiply));
        } else if (c == '/') {
            list.push_back(Object::new_operator(Operator::Divide));
        } else if (c == '^') {
            list.push_back(Object::new_operator(Operator::Power));
        } else if (c == '-') {
            list.push_back(Object::new_operator(Operator::Minus));
        } else if (is_number(c)) {
            buffer[0] = c;
            int j = 1;
            while (i+1<len && (s[i+1]=='.' || is_number(s[i+1]))) {
                buffer[j] = s[i+1];
                j++;
                i++;
            }
            buffer[j] = '\0';
            list.push_back(Object::new_variable(map.insert(new Number(buffer))));
        }
    }
    delete[] buffer;
    return std::make_tuple(InfixExpression(fix(std::move(list))), std::move(map), c_var);
}

Number Calculator::evaluate(const char *input) {
    char *s = clean(input);
    if (s == nullptr) throw Exception("Invalid input");
    auto p = parse(s);
    delete[] s;

    if (!std::get<0>(p).is_valid()) throw Exception("Invalid input");
    Number result = std::get<0>(p).to_posfix().evaluate(std::move(std::get<1>(p)));

    char c = std::get<2>(p);
    if (c != '\0') {
        var[c-'A'] = new Number(result);
    }

    return result;
}
