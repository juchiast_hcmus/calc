# Thông tin về project

Ngoài các yêu cầu của đồ án, em có mở rộng thêm những tính năng sau:

- Tính lũy thừa với số thực lớn
- Tự thêm dấu nhân vào những nơi phù hợp
- Tự thay thế các dấu + và - liên tiếp thành dấu phù hợp
- Cho phép lưu biến và sử dụng biến, (tên biến là 1 chữ cái từ A đến Z)
- Cho phép sử dụng hàm số và hằng số có sẵn
